from notebook import Note, Notebook;
import sys;

class Menu:
    def __init__(self):
        self.notebook = Notebook();
        self.notes = None;
        self.choices = {"1":self.showNotes,
                        "2":self.searchNotes,
                        "3":self.addNote,
                        "4":self.modifyNote,
                        "5":self.quit}
        return ;
    def displayMenu(self):
        print("""
        Notebook Menu
        1. Show all Notes
        2. Search Notes
        3. Add Note
        4. Modify Note
        5. Quit
        """)
        return ;
    def run(self):
        while True :
            self.displayMenu();
            choice = input("Enter an option : ");
            action = self.choices.get(str(choice));
            print action;
            if action:
                action();
            else:
                print ("{0} is not a valid choice".format(choice));
        return ;
    def showNotes(self):
        if not self.notes:
            self.notes = self.notebook.notes;
        for note in self.notes:
            print ("{0}: {1}\n{2}".format(note.id, note.tags, note.memo));
        return ;
    def searchNotes(self):
        filterSearch = input("Search for : ");
        notes = self.notebook.search(filterSearch);
        self.showNotes(notes);
        return ;
    def addNote(self):
        memo = input("Enter a memo : ");
        self.notebook.new_note(memo);
        print ("Your note has been added.");
        return ;
    def modifyNote(self):
        id = input("Enter a note id : ");
        memo = input("Enter a memo : ");
        tags = input("Enter tags : ");
        if memo:
            self.notebook.modify_memo(id, memo);
        if tags:
            self.notebook.modify_tags(id, tags);
        return ;
    def quit(self):
        print "Thanks.";
        sys.exit(0);

if __name__ == "__main__":
    Menu().run();