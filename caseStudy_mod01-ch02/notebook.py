"""
import sys
sys.path
sys.path.append("D:\MOH3N\wip\Python Master Art Design Patterns\caseStudy_mod01-ch02")
import maya.cmds as cmds
if not cmds.commandPort(':4434', q=True):
    cmds.commandPort(n=':4434')
"""
import datetime;

# Store the next available id for all new notes
last_id = 0;


class Note:
    """Represent a note in the notebook. Match against a 
    string in searches and store tags for each note."""

    def __init__(self, memo, tags=''):
        """initialize a note with memo and optional
        space-separated tags. Automatically set the note's
        creation date and a unique id."""
        self.memo = memo;
        self.tags = tags;
        self.creationDate = datetime.date.today();
        global last_id;
        last_id += 1;
        self.id = last_id;

    def match(self, filter_string):
        """Determine if this note matches the filter
        text. Return True if it matches, False otherwise.
        Search is case sensitive and matches both text and
        tags."""
        return filter_string in self.memo or filter_string in self.tags;


class Notebook:
    """Represent a collection of notes that can be tagged, modified, and searched."""

    def __init__(self):
        """Initialize a notebook with an empty list."""
        self.notes = [];

    def new_note(self, memo, tags=""):
        """Create a new note and add it to the list."""
        self.notes.append(Note(memo, tags));
        return;

    def _findNote(self, note_id):
        for note in self.notes:
            if note.id == note_id:
                return note;
        return None;

    def modify_memo(self, note_id, memo):
        """Find the note with the given id and change its memo to the given value."""
        self._findNote(note_id).memo = memo;
        return;

    def modify_tags(self, note_id, tags):
        """Find the note with the given id and change its tags to the given value."""
        self._findNote(note_id).tags = tags;
        return;

    def search(self, filter_string):
        return [note for note in self.notes if note.match(filter_string)]


if __name__ == "__main__":
    from notebook import Note;

    n1 = Note("First");
    n2 = Note("Second");
    print n2;
    print n2.id;
